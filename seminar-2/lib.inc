section .text
 
; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    cmp byte [rdi+rax], 0
    je .end
    inc rax
    jmp .loop
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi              ; Сохраняем rdi перед вызовом string_length
    call string_length    ; Получаем длину строки в rax
    pop  rsi              ; Восстанавливаем rdi и одновременно перемещаем его в rsi
    mov  rdx, rax         ; Длина строки
    mov  rax, 1
    mov  rdi, 1
    syscall
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax          ; Clear rax (result)
    xor rdx, rdx          ; Clear rdx (length of number)
.loop:
    ; Load character from string into cl
    movzx rcx, byte [rdi + rdx]

    ; Check if it is a digit
    test cl, cl
    jz .end               ; Jump to the end of string
    sub cl, '0'           ; Convert ASCII to integer
    cmp cl, 9             ; Check if it is a valid digit
    ja .end               ; Jump to failed if not a digit

    ; Add digit to result
    imul rax, rax, 10     ; Multiply result by 10
    add rax, rcx          ; Add digit to result

    inc rdx               ; Increment the length of number
    jmp .loop             ; Continue loop

.end:
    ret                   ; Return
