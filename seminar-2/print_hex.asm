section .data
    codes db '0123456789ABCDEF'
    newline db 0xA

section .text
global _start

print_newline:
    mov rax, 1
    mov rdi, 1
    mov rsi, newline
    mov rdx, 1
    syscall
    ret

print_hex:
    mov  rcx, 64
.loop:
    push rdi
    sub  rcx, 4
    sar  rdi, cl
    and  rdi, 0xf
    lea  rsi, [codes + rdi]
    mov  rax, 1
    mov  rdi, 1
    mov  rdx, 1
    push rcx
    syscall
    pop  rcx
    pop rdi
    test rcx, rcx
    jnz .loop
    ret

print_variables:
    sub  rsp, 24  ; выделение места под три локальные переменные
    mov  qword [rsp], 0xAA  ; запись значения в первую переменную
    mov  qword [rsp + 8], 0xBB  ; во вторую
    mov  qword [rsp + 16], 0xFF  ; и в третью
    
    mov  rdi, [rsp]  ; передача первой переменной в print_hex
    call print_hex
    call print_newline
    mov  rdi, [rsp + 8]  ; передача второй переменной
    call print_hex
    call print_newline
    mov  rdi, [rsp + 16]  ; передача третьей переменной
    call print_hex
    call print_newline
    
    add  rsp, 24  ; освобождение памяти
    ret  ; завершение функции

_start:
    call print_variables
    mov  rax, 60  ; syscall: exit
    xor  rdi, rdi  ; exit code 0
    syscall
